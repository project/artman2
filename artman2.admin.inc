<?php

/**
 * @file
 * Admin page callbacks for the artman2 module.
 */

/**
 * Menu callback; displays the artman2 module settings page.
 *
 * @ingroup forms
 * @see system_settings_form()
 */
function artman2_admin_settings() {
  include_once './includes/install.inc';

  $db_url = artman2_db_url_parse(variable_get('artman2_db_url', ''));
  $db_path = $db_url['path'];
  $db_type = $db_url['scheme'];
  $db_host = ($db_url['host']) ? $db_url['host'] : 'localhost';
  $db_user = $db_url['user'];
  $db_pass = $db_url['pass'];
  $db_port = $db_url['port'];

  $db_types = drupal_detect_database_types();

  // If both 'mysql' and 'mysqli' are available, we disable 'mysql':
  if (isset($db_types['mysqli'])) {
    unset($db_types['mysql']);
  }
  $form['artman2_db_url'] = array(
    '#type' => 'hidden',
    '#value' => variable_get('artman2_db_url', ''),
  );

  $form['artman2_db'] = array(
    '#type' => 'fieldset',
    '#title' => t('Article Manager 2 Database'),
    '#collapsible' => ($db_path == '') ? FALSE : TRUE,
    '#collapsed' => ($db_path == '') ? FALSE : TRUE,
  );

  if (count($db_types) > 1) {
    $form['artman2_db']['db_type'] = array(
      '#type' => 'radios',
      '#title' => t('Database type'),
      '#required' => TRUE,
      '#options' => $db_types,
      '#default_value' => ($db_type ? $db_type : current($db_types)),
    );
  }
  else {
    if (count($db_types) == 1) {
      $db_types = array_values($db_types);
      $form['artman2_db']['db_type'] = array(
        '#type' => 'hidden',
        '#value' => $db_types[0],
      );
    }
  }

  // Database name
  $form['artman2_db']['db_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Database name'),
    '#default_value' => $db_path,
    '#size' => 45,
    '#maxlength' => 45,
    '#required' => TRUE,
  );

  // Database username
  $form['artman2_db']['db_user'] = array(
    '#type' => 'textfield',
    '#title' => t('Database username'),
    '#default_value' => $db_user,
    '#size' => 45,
    '#maxlength' => 45,
    '#required' => TRUE,
  );

  // Database username
  $form['artman2_db']['db_pass'] = array(
    '#type' => 'textfield',
    '#title' => t('Database password'),
    '#default_value' => $db_pass,
    '#size' => 45,
    '#maxlength' => 45,
  );

  // Database host
  $form['artman2_db']['db_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Database host'),
    '#default_value' => $db_host,
    '#size' => 45,
    '#maxlength' => 45,
    '#required' => TRUE,
    '#description' => t('If your database is located on a different server, change this.'),
  );

  // Database port
  $form['artman2_db']['db_port'] = array(
    '#type' => 'textfield',
    '#title' => t('Database port'),
    '#default_value' => $db_port,
    '#size' => 45,
    '#maxlength' => 45,
    '#description' => t('If your database server is listening to a non-standard port, enter its number.'),
  );

  // Table prefix
  $form['artman2_db']['artman2_db_prefix'] = array(
    '#type' => 'textfield',
    '#title' => t('Table prefix'),
    '#default_value' => variable_get('artman2_db_prefix', 'db_'),
    '#size' => 45,
    '#maxlength' => 45,
  );

  if ($db_path != '') {
    // Collect some stats
    $processed = db_result(db_query('SELECT COUNT(*) FROM {artman2} WHERE last_updated_time > 0'));
    _artman2_db_connect();
    db_set_active('artman2');
    $total = db_result(db_query('SELECT COUNT(*) FROM {article}'));
    db_set_active();
    $remaining = $total - $processed;

    $count = format_plural($remaining, 'There is 1 article left to migrate.', 'There are @count articles left to migrate.');
    $percentage = ((int)min(100, 100 * ($total - $remaining) / max(1, $total))) .'%';

    $form['artman2_migrate'] = array(
      '#type' => 'checkbox',
      '#title' => 'Migrate during cron maintenance tasks',
      '#default_value' => variable_get('artman2_migrate', FALSE),
      '#description' => t('%percentage of the articles have been migrated.', array('%percentage' => $percentage)) .' '. $count,
    );
    // Migration throttle:
    $form['artman2_cron_limit'] = array(
      '#type' => 'select',
      '#title' => t('Number of articles to migrate per cron run'),
      '#default_value' => variable_get('artman2_cron_limit', 10),
      '#options' => drupal_map_assoc(array(10, 20, 50, 100, 200, 500)),
      '#description' => t('The maximum number of articles migrated in each pass of a <a href="@cron">cron maintenance task</a>. If necessary, reduce the number of items to prevent timeouts and memory errors while migrating.', array('@cron' => url('admin/reports/status')))
    );
  }

  $items = array();
  $types = node_get_types();
  foreach ($types as $type) {
    $items[$type->type] = $type->name;
  }

  $form['artman2_default_node_type'] = array(
    '#type' => 'select',
    '#title' => t('Default node type for imported articles'),
    '#default_value' => variable_get('artman2_default_node_type', 'story'),
    '#options' => $items,
  );

  $items = array();
  $formats = filter_formats();
  foreach ($formats as $format) {
    $items[$format->format] = $format->name;
  }
  $form['artman2_default_format'] = array(
    '#type' => 'select',
    '#title' => t('Default format for imported articles'),
    '#default_value' => variable_get('artman2_default_format', variable_get('filter_default_format', 1)),
    '#options' => $items,
  );

  $form['artman2_upload_path'] = array(
    '#type' => 'textfield',
    '#title' => t('Article Manager 2 upload path/URL'),
    '#default_value' => variable_get('artman2_upload_path', ''),
    '#description' => t('The URL or path (relative to Drupal root) where images and uploaded files are located.'),
  );
  return system_settings_form($form);
}

function artman2_db_url_parse($db_url) {
  $encoded_url = parse_url($db_url);
  foreach ($encoded_url as $key => $value) {
    if ($key == 'path') {
      $url[$key] = ltrim(urldecode($value), '/');
    }
    else {
      $url[$key] = isset($value) ? urldecode($value) : '';
    }
  }
  return $url;
}

/**
 * Form API validate for install_settings form.
 */

function artman2_admin_settings_validate($form, &$form_state) {
  $db_type = $form_state['values']['db_type'];
  $db_host = $form_state['values']['db_host'];
  $db_user = $form_state['values']['db_user'];
  $db_pass = $form_state['values']['db_pass'];
  $db_port = $form_state['values']['db_port'];
  $db_path = $form_state['values']['db_path'];

  // Verify the table prefix
  if (!empty($form_state['values']['artman2_db_prefix']) && is_string($form_state['values']['artman2_db_prefix']) && !preg_match('/^[A-Za-z0-9_.]+$/', $form_state['values']['artman2_db_prefix'])) {
    form_set_error('db_prefix', t('The database table prefix you have entered, %db_prefix, is invalid. The table prefix can only contain alphanumeric characters, periods, or underscores.', array('%db_prefix' => $form_state['values']['artman2_db_prefix'])), 'error');
  }

  if (!empty($db_port) && !is_numeric($db_port)) {
    form_set_error('db_port', t('Database port must be a number.'));
  }
  $databases = drupal_detect_database_types();
  if (!in_array($db_type, $databases)) {
    form_set_error('db_type', t("In your %settings_file file you have configured @drupal to use a %db_type server, however your PHP installation currently does not support this database type.", array('%settings_file' => $settings_file, '@drupal' => drupal_install_profile_name(), '%db_type' => $db_type)));
  }
  else {
    // Verify
    $db_url = $db_type .'://'. urlencode($db_user) . ($db_pass ? ':'. urlencode($db_pass) : '') .'@'. ($db_host ? urlencode($db_host) : 'localhost') . ($db_port ? ":$db_port" : '') .'/'. urlencode($db_path);
    if (isset($form)) {
      form_set_value($form['artman2_db_url'], $db_url, $form_state);
    }
  }
}

/**
 * Form API submit for install_settings form.
 */

function artman2_admin_settings_submit($form, &$form_state) {
  unset($form_state['values']['db_type']);
  unset($form_state['values']['db_host']);
  unset($form_state['values']['db_user']);
  unset($form_state['values']['db_pass']);
  unset($form_state['values']['db_port']);
  unset($form_state['values']['db_path']);
}